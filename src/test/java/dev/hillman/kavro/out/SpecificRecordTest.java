package dev.hillman.kavro.out;

import dev.hillman.avro.BottleMessage;
import dev.hillman.avro.MessageType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecordBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SpecificRecordTest {

  public BottleMessage createSpecificRecord() {
    var bottle =
        BottleMessage.newBuilder()
            .setMessage("jabbadabbadoo")
            .setMessageId(123)
            .setMessageType(MessageType.values()[2])
            .setMessageSender("Flip")
            .build();
    return bottle;
  }

  public byte[] encode(SpecificRecordBase record) {
    DatumWriter<SpecificRecordBase> genericDatumWriter = new SpecificDatumWriter<>(record.getSchema());
    EncoderFactory encoderFactory = EncoderFactory.get();

    ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    try {
      BinaryEncoder reuseEncoder = encoderFactory.binaryEncoder(byteStream, null);
      genericDatumWriter.write(record, reuseEncoder);
      reuseEncoder.flush();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return byteStream.toByteArray();
  }

  @Test
  public void testGenericEncodeSpecificDecode() {
    BottleDeserializer deserializer = new BottleDeserializer();

    BottleMessage input = createSpecificRecord();
    byte[] encoded = encode(input);

    BottleMessage output = deserializer.deserialize("a", encoded);
    Assertions.assertEquals(input, output);
  }

}

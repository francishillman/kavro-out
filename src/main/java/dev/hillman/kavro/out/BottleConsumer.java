package dev.hillman.kavro.out;

import dev.hillman.avro.BottleMessage;
import java.time.Duration;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class BottleConsumer {
  private static final Logger logger = Logger.getLogger(BottleConsumer.class.getCanonicalName());

  private Properties props;
  private KafkaConsumer<String, BottleMessage> consumer;
  private static final int EARLY_STOP = 3;
  private int emptyResponses = 0;


  public BottleConsumer() {
    props = new Properties();
    props.setProperty("bootstrap.servers", "100.70.77.61:9092");
    props.setProperty("group.id", "kavro-out-2");  // 1 for nothing, 2 for 2
    props.setProperty("enable.auto.commit", "false");
    props.setProperty(
        "key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
    props.setProperty(
        "value.deserializer", "dev.hillman.kavro.out.BottleDeserializer");
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

    consumer = new KafkaConsumer<>(props);
    consumer.subscribe(Set.of("kavro-2"));
  }

  /**
   * Try to ingest x times some data
   *
   * @param requests how many times to try and fetch some data
   */
  public void ingestData(int requests) {
    for (int i = 0; i < requests; i++) {
      ConsumerRecords<String, BottleMessage> records = consumer.poll(Duration.ofSeconds(10));
      if (records.count() == 0) {
        logger.info("Polled 0 messages");
        emptyResponses++;
        if (emptyResponses >= EARLY_STOP) {
          logger.info("Early stopping after " + emptyResponses + " empty responses");
          return;
        }
      }
      for (ConsumerRecord<String, BottleMessage> record : records) {
        logger.info(
            "Record received from " + record.key() + " - " + record.value().toString());
      }
      consumer.commitSync();
    }
  }

  public void close() {
    consumer.close();
  }
}

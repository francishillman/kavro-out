package dev.hillman.kavro.out;

import java.util.logging.Logger;

public class Main {
  private static final Logger logger = Logger.getLogger(Main.class.getCanonicalName());
  private static final int VERSION = 1;

  public static void main(String[] args) {
    logger.info("Kavro data consumer started with data version: " + VERSION);
    BottleConsumer consumer = new BottleConsumer();

    consumer.ingestData(30);

    consumer.close();

    logger.info("Kavro data publisher finished");
  }

}

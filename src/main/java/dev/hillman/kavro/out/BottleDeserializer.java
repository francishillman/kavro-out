package dev.hillman.kavro.out;

import dev.hillman.avro.BottleMessage;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.kafka.common.serialization.Deserializer;

public class BottleDeserializer implements Deserializer<BottleMessage> {

  DatumReader<BottleMessage> bottleMessageDatumReader =
      new SpecificDatumReader<>(BottleMessage.class);
  DecoderFactory decoderFactory = DecoderFactory.get();

  @Override
  public BottleMessage deserialize(String s, byte[] bytes) {
    BinaryDecoder reuseDecoder = decoderFactory.binaryDecoder(bytes, null);
    try {
      return bottleMessageDatumReader.read(null, reuseDecoder);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
